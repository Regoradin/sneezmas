import numpy as np
import seaborn as sns
import pandas as pd

import geopandas
from shapely.geometry import Point

import json

import matplotlib.pyplot as plt


sneezes = {}
sneezes["times"] = []
sneezes["day_of_year"] = []
sneezes["year"] = []
sneezes["time_since_previous"] = []

for filename in ["Sneeze Data Year 1.txt", "Sneeze Data Year 2.txt"]:
    f = open(filename)
    sneeze_data = f.readlines()
    f.close()

    start_time = pd.to_datetime(sneeze_data[2][38:])
    for i in range (len(sneeze_data)-1, -1, -1):
        if sneeze_data[i].startswith("Lap Total Time: "):
            delta = pd.to_timedelta(sneeze_data[i][16:].strip())
            sneeze_time = delta + start_time
            
            sneezes["times"].append(sneeze_time)
            sneezes["day_of_year"].append((sneeze_time).strftime('%m-%d'))
            sneezes["year"].append((sneeze_time).strftime('%y'))
            if len(sneezes["times"]) > 1:
                sneezes["time_since_previous"].append(sneeze_time - sneezes["times"][-2])
            else:
                sneezes["time_since_previous"].append(sneeze_time - pd.to_datetime(0))

#time_range = ((pd.to_datetime("Jan 1 2018") - pd.to_datetime(1970,1,1)).days,(pd.to_datetime("Dec 31 2018") - pd.to_datetime(1970,1,1)).days)

#SNEEZES HISTOGRAM
#sns.histplot(data=sneezes, x="times", binwidth=1, element="poly", binrange=(17532, 17532+365))
#sns.histplot(data=sneezes, x="times", binwidth=1, element="poly", binrange=(17532+365, 17532+365+365))
#plt.show()

#SNEEZES YEAR BY YEAR
#sns.histplot(data=sneezes, x="day_of_year", binwidth=1, hue="year", multiple="layer", element="poly", discrete=False)
#plt.show()

#TIME BETWEEN SNEEZES
#sns.histplot(data=sneezes, x="time_since_previous", element="poly", discrete = False)
#plt.show()

states = geopandas.read_file('cb_2018_us_cbsa_500k.shp')
states.to_crs("EPSG:3395")
base_map = states.plot()
base_map.set_xlim([-130, -65])
base_map.set_ylim([20, 55])

points = geopandas.GeoSeries([Point(-122.262, 47.653), Point(-87.442, 41.87)])
points.set_crs("EPSG:3395")
points.plot(ax=base_map, color='red', markersize=5)

f = open('Takeout\Location History\Location History.json')
location_history = json.load(f)
f.close()

locations = []
print(location_history["locations"][0])
for location in location_history["locations"]:
    locations.append(Point(location["longitudeE7"]/10000000, location["latitudeE7"]/10000000))
    #locations.append(Point(location["latitudeE7"]/10000000, location["longitudeE7"]/10000000))

locations = geopandas.GeoSeries(locations)
locations.set_crs("EPSG:3395")
locations.plot(ax=base_map, color='red', markersize=3)
plt.show()


# data = {}
# data["x_test"] = [1, 5, 2, 5]
# data["y_test"] = [10, 20, 30, 40]
# sns.relplot(x="x_test", y="y_test", data=data)

# plt.show()
